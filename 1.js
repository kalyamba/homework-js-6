// 1. це процес забезпечення безпеки програм, обмеження можливостей зловживання та недопущення втручання у приватну частину програми.

//    2. function declaration оголошення функції за допомогою ключового слова function.
// function expression оголошення функції шляхом присвоєння функції змінній. 

//     3. це механізм, який дозволяє використовувати змінні та функції до їх оголошення в коді.У JavaScript.

function createNewUser() {
  const firstName = prompt("Введите имя:");
  const lastName = prompt("Введите фамилию:");
  const birthdayString = prompt("Введите дату рождения в формате dd.mm.yyyy:");
  const [day, month, year] = birthdayString.split(".");
  const birthday = new Date(year, month - 1, day);

  return {
    firstName,
    lastName,
    birthday,
    getAge() {
      const today = new Date();
      const birthDate = this.birthday;
      let age = today.getFullYear() - birthDate.getFullYear();
      const monthDiff = today.getMonth() - birthDate.getMonth();
      if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < birthDate.getDate())) {
        age--;
      }
      return age;
    },
    getPassword() {
      const firstLetter = this.firstName.charAt(0).toUpperCase();
      const year = this.birthday.getFullYear();
      const lastNameLower = this.lastName.toLowerCase();
      return `${firstLetter}${lastNameLower}${year}`;
    },
    getLogin() {
      return `${this.firstName.toLowerCase()} ${this.lastName.toLowerCase()}`;
    }
  };
}

const user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());